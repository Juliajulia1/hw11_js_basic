/*
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */
const btn = document.querySelector('button')
const section = document.querySelector('section')
console.log(section)
function createParagraph() {
    const paragraph = document.createElement("p");
    paragraph.innerText = 'New Paragraph';
    btn.before(paragraph);
    btn.removeEventListener('click', createParagraph);
}

btn.addEventListener('click', createParagraph)

const newBtn = document.createElement('button');
newBtn.innerText = 'Create input';
newBtn.setAttribute('id', 'btn-input-create');
newBtn.classList.add('created-btn');
section.style.textAlign = 'center';
section.appendChild(newBtn);

function createInput() {
    const input = document.createElement("input");
    input.setAttribute('type', 'text')
    input.setAttribute('placeholder','This is NEW content on our landing page')
    input.setAttribute('style', 'width:90%; margin-top: 20px;box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);padding: 10px;')
    newBtn.after(input)
    newBtn.removeEventListener('click', createInput);
}

newBtn.addEventListener('click', createInput)





